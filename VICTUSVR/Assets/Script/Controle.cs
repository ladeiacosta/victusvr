﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Elementos da Interface Gráfica
using UnityEngine.AI;
using System.IO.Ports; // Biblioteca para ler comunicação serial com Arduino

public class Controle : MonoBehaviour {

	SerialPort serial = new SerialPort("COM5",9600); //define a porta
	public Text displayContagem, displayBatimentos, displayVelocidade, displayEmg, displayObjetivo;
	public float tempoSegundos, tempoMinutos, tempo=0.0f;
	public float fimDaPartida=4000;
	public NavMeshAgent navmesh;
	public GameObject player;
	public Text tempoFim;
	public GameObject entrada;
	public AudioSource bike, musica;



	// Use this for initialization
	void Start () {
		Time.timeScale = 0;
		serial.Open ();
		serial.ReadTimeout = -1;
		navmesh = player.GetComponent<NavMeshAgent> ();
		//StartCoroutine (LerDadosDoSerial ());// começa o loop
		displayVelocidade.text = "0";
		displayBatimentos.text = " ";

		//fimDaPartida = getda interface

	}

	string emg, bpm;// variáveis 
	float velocidade, eixo;
	// Update is called once per frame
	void Update () {
			if (tempo >= fimDaPartida) {
			//fim da partida
				displayObjetivo.text = "Acabou a sessão!";
				Time.timeScale = 0;
			bike.Pause ();
			musica.Pause ();
				//******INSTANTIATE OS SCORES
			} else {

			tempo += Time.deltaTime;
			tempoMinutos = ((int)tempo / 60) ;
			tempoSegundos = (int)tempo % 60;
			displayContagem.text = tempoMinutos.ToString("00")+ ":"+tempoSegundos.ToString("00");
			//Debug.Log("Em cima" + serial.ReadLine());
		/*if (velocidade > 0) {	
			velocidade= velocidade - 0.15f;
			displayVelocidade.text = velocidade.ToString ("00");
			navmesh.speed = (float)(velocidade / 3.6);
		}*/
			string[] valores = serial.ReadLine ().Split ('#'); // separador de valores
			serial.BaseStream.Flush ();
			
			bpm = valores[0];
			velocidade = 1.5f * float.Parse(valores [1]);
			emg = valores [2];
			//eixo = valores [3];
			Debug.Log("Valores" + "#"+bpm +"#"+ velocidade +"#"+ emg);
			
			displayBatimentos.text = bpm;
			displayEmg.text = "EMG: "+emg;
			displayVelocidade.text = velocidade.ToString("00");
			
			navmesh.speed = (float)(velocidade / 3.6);
			
			
			


		
		}
	}
		
	IEnumerator LerDadosDoSerial(){
		while(true){
			string[] valores = serial.ReadLine ().Split ('#'); // separador de valores
			bpm = valores[0];
			velocidade = float.Parse(valores [1]);
			emg = valores [2];
			//eixo = valores [3];
			Debug.Log("Valores" + bpm + velocidade + emg);
			yield return new WaitForSeconds (.03f); //tempo de leitura de novas informações
		}
	}

	public void SetaTempo(){
		fimDaPartida =60* float.Parse(tempoFim.text);
		Time.timeScale = 1;
		musica.Play ();
		bike.Play ();
		Destroy(entrada);
	}

}
